#!/usr/bin/python3

import string
import re
import os

with open('input', 'r') as f:
    text = f.read()

print(text)
print(len(text))

words = text.split()
words = [re.sub("[aeiou]+", "", word.translate(str.maketrans('', '', string.punctuation)).lower()) for word in words]
words = [word for word in words if word != '']
#print(words)

freq = {}
for word in words:
    if word not in freq: freq[word] = 0
    freq[word] += 1
#print(dict(sorted(freq.items(), key=lambda item: item[1])))

replace = {}
cur = ord('A')
ans = ''
for word, cnt in freq.items():
    if len(word)+1+cnt < len(word)*cnt:
        ans += chr(cur) + word
        replace[word] = chr(cur)
        cur += 1

for word in words:
    if word in replace:
        ans += replace[word]
    else:
        ans += word

print(ans)
print(len(ans))

with open('output', 'w') as f:
    f.write(ans)

os.system('7z a -mm=Deflate -mfb=258 -mpass=15 -r output.zip output')