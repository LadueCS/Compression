# Compression Project

Now that you've had a taste of compression, try applying these ideas to a real-world problem! **You will be given a text file, and your job is to write a program to compress the text while losing as little information as possible!**

## Rules
- You will have one hour to write your program.
- You can use any language.
- You can use pre-written code and existing libraries.
- You can write a decompression program if you would like. This is recommended if you have time.
- After one hour is up, your program will be run on a secret test input, and you will be given the output of your program.
- You should decompress your output, and then answer a question about the text.
- If you get the question wrong, your compression algorithm probably lost too much information and you will be disqualified.
- The team with the smallest output file that still correctly answers the question wins!

## Ideas to get you started
- Remove punctuation?
- Remove spaces?
- Remove vowels?
- Replace commonly occurring words with something shorter?
- Binary outputs???
- **Be creative!!**