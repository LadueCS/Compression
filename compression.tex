\documentclass{beamer}

\usetheme{Berkeley}
\usecolortheme{beaver}

% set font encoding for PDFLaTeX, XeLaTeX, or LuaTeX
\usepackage{ifxetex,ifluatex}
\if\ifxetex T\else\ifluatex T\else F\fi\fi T%
  \usepackage{fontspec}
\else
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{lmodern}
\fi

\usepackage{hyperref}

\title{The Magic of Compression}
\author{Anthony Wang}
\logo{\includegraphics[height=1.5cm]{logo}}

% Enable SageTeX to run SageMath code right inside this LaTeX file.
% http://doc.sagemath.org/html/en/tutorial/sagetex.html
% \usepackage{sagetex}

% Enable PythonTeX to run Python – https://ctan.org/pkg/pythontex
% \usepackage{pythontex}

\begin{document}

\frame{\titlepage}


\begin{frame}
\frametitle{Compression? Boring!}

\begin{itemize}
\item Compression's not a topic that people get excited about.
\item You guys want to write AIs to take over the world, right?
\item So why are we talking about compression today?
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Consider this:}

\begin{itemize}
\item We don't appreciate the importance of compression.
\item What if compression did not exist?
\item What would the world be like? Computers? The Internet?
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Let's start with images}

\begin{itemize}
\item Images are \alert{2D grids of pixels}.
\item Each pixel is a \alert{3-byte value}, with byte 1 representing the amount of red, byte 2 green, and byte 3 blue.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{An example}

\begin{itemize}
\item The following image has dimensions $1920x1080$, with a total file size of $1920 \cdot 1080 \cdot 3 B = 6.2208 MB$.
\end{itemize}

\includegraphics[width=\textwidth]{image.jpg} % Technically image.bmp is the 6 MB file
\end{frame}


\begin{frame}
\frametitle{Is $6 MB$ bad?}
\begin{itemize}
\item At first, it doesn't seem too bad, right?
\item But what if we have $100$ images? Now it's $622 MB$. That's about the same as the file size of Wii Sports.
\item With $1000$ images, it's $6 GB$, the same size as the original Star Wars trilogy movies combined. % I don't know the exact size, but that sounds about right for a video of that length
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Can we do better?}

\begin{itemize}
\item Instead of storing "black pixel, black pixel, ...", let's store "1729 black pixels".
\item Tricks like these can get the file size of our image down to only $53.5 KB$, a ratio of more than $112$! Wow!
\end{itemize}

\includegraphics[width=\textwidth]{image.jpg} % See image.avif for the highly compressed file
\end{frame}


\begin{frame}
\frametitle{If that didn't shock you...}
\begin{itemize}
\item Now consider a 100-minute movie with 60 frames per second. Each frame will be 1920x1080.
\item How big is that? $6 MB * 60 \frac{frames}{second} * 60 \frac{seconds}{minute} * 100 \frac{minutes} = 2160 GB$!
\item Our computers have $128 GB$ hard drives, so we need $16$ computers to store the entire video!!
\item But a typical 100-minute movie is only $2 GB$ with a compression ratio of over $1000$!
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{This is the magic of compression!}

\begin{itemize}
\item We generate, transfer, and store tons of data every day.
\item It's crucial that we handle the data in the most efficient way possible.
\item \alert{Compression is everywhere}: images, audio, videos, files, and networking.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Can compress our image more?}

\begin{itemize}
\item Yes --- if we sacrifice something!
\item This is called \alert{lossy} compression, where some information is lost, as opposed to \alert{lossless} compression.
\item For images, we can exploit limitations in the human eye to tweak colors for better compression.
\item This is what JPEG does, at the expense of slightly off colors if you look very very closely.
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Compression + AI!}

\begin{itemize}
\item There's a special kind of AI called an autoencoder, which \alert{learns to compress data on its own!}
\item In an autoencoder, two neural networks team up, one that compresses the data and one that decompresses the data.
\item AI research promises an exciting future for compression!
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Some interesting reads}

\begin{itemize}
\item \url{https://chipsandcheese.com/2021/01/30/modern-data-compression-in-2021-part-1-a-simple-overview-on-the-art-of-image-encoding/}
\item \url{https://chipsandcheese.com/2021/02/28/modern-data-compression-in-2021-part-2-the-battle-to-dethrone-jpeg-with-jpeg-xl-avif-and-webp/}
\end{itemize}
\end{frame}


\end{document}
